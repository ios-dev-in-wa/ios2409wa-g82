//
//  ViewController.swift
//  ios2409ls17
//
//  Created by WA on 19.11.2020.
//

import UIKit
import AVKit
import CoreLocation

class ViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
//        collectionView.dataSource = self
        
//        collectionView.delegate = self
//        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
//           print("DEFAULT LAYOUT")
//        }
//        print(UserDefaults.standard.string(forKey: "name_preference"))
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        let controller = AVPlayerViewController()
//        let url = Bundle.main.url(forResource: "videoExample", withExtension: "mov")!
//        let item = AVPlayerItem(url: url)
//        controller.player = AVPlayer(playerItem: item)
//        AVPlayerLayer(player: AVPlayer(playerItem: item))
//        present(controller, animated: true, completion: nil)
//        AVAudioSession.sharedInstance().set
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { response in
            if response {
                //access granted
            } else {
                
            }
        }
    }
    
    @IBAction func openSettingsAction(_ sender: UIBarButtonItem) {
        let manager = CLLocationManager()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            manager.requestWhenInUseAuthorization()
            manager.startUpdatingLocation()
        }
//        print(UIApplication.openSettingsURLString)
//        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
//            return
//        }
//
//        if UIApplication.shared.canOpenURL(settingsUrl) {
//            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
//                print("Settings opened: \(success)") // Prints true
//            })
//        }
    }
}

extension ViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 11
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LabelCell", for: indexPath) as? LabelCollectionViewCell else { fatalError() }
        cell.textLabel.text = "row:\(indexPath.row) \((indexPath.row) % 3 == 0 ? "LOOOOOOOOONG" : "")"
        cell.backgroundColor = .random
        return cell
    }
}

extension CGFloat {
    static var random: CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

extension UIColor {
    static var random: UIColor {
        return UIColor(red: .random, green: .random, blue: .random, alpha: 1.0)
    }
}
