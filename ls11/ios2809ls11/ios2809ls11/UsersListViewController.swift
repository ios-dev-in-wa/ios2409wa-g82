//
//  UsersListViewController.swift
//  ios2809ls11
//
//  Created by WA on 29.10.2020.
//

import UIKit
import Kingfisher

struct User {
    let name: String
    let username: String
    let imageUrl: String
}

class UsersListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    var users = [
        User(name: "Ihor", username: "iho4", imageUrl: "https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg"),
        User(name: "Ihor", username: "iho4", imageUrl: "https://www.eharmony.co.uk/dating-advice/wp-content/uploads/2011/04/profilephotos-960x640.jpg"),
        User(name: "Ihor", username: "iho4", imageUrl: ""),
        User(name: "Ihor", username: "iho4", imageUrl: ""),
        User(name: "Ihor", username: "iho4", imageUrl: "")
    ]

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        DispatchQueue.main.async {
            print("DDDHeelo async")
        }
        print("DDDhello sync")
    }
}

extension UsersListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "UserTableViewCell", for: indexPath) as? UserTableViewCell else { fatalError() }
        cell.titleLabel?.text = users[indexPath.row].name
        cell.subtitleLabel?.text = users[indexPath.row].username
        if let url = URL(string: users[indexPath.row].imageUrl) {
            cell.profileImageView.kf.indicatorType = .activity
            cell.profileImageView.kf.setImage(with: url)
        }
        return cell
    }
}

//extension UIImageView {
//    var kf: Kingfisher
//}
