//
//  DrawView.swift
//  ios2809ls11
//
//  Created by WA on 29.10.2020.
//

import UIKit

class DrawView: UIView {

    override func draw(_ rect: CGRect) {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 50, y: 50))
        path.addLine(to: CGPoint(x: 100, y: 50))
        path.addLine(to: CGPoint(x: 100, y: 150))
        path.addLine(to: CGPoint(x: 50, y: 100))
        path.addLine(to: CGPoint(x: 50, y: 50))
        path.close()
//        path.addArc(withCenter: <#T##CGPoint#>, radius: <#T##CGFloat#>, startAngle: <#T##CGFloat#>, endAngle: <#T##CGFloat#>, clockwise: <#T##Bool#>)
        UIColor.red.setFill()
        path.fill()
        
        path.lineWidth = 2
    }
}
