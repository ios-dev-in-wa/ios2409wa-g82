//
//  ViewController.swift
//  ios2809ls11
//
//  Created by WA on 29.10.2020.
//

import UIKit
import ProgressHUD

class ViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!

    override func viewDidLoad() {
        super.viewDidLoad()
//        scrollView.contentSize
//        scrollView.contentInset
//        NotificationCenter.default.post(name: UIResponder.keyboardWillShowNotification, object: <#T##Any?#>, userInfo: <#T##[AnyHashable : Any]?#>)
//        Settings.shared.appStartCount
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keybordWillHide), name: UIResponder.keyboardDidHideNotification, object: nil)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let drawBox = DrawView()
        drawBox.backgroundColor = .white
        drawBox.frame = CGRect(x: 50, y: 50, width: 400, height: 400)
        view.addSubview(drawBox)

        ProgressHUD.show("Testing", icon: .bookmark, interaction: true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("test")
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    @objc func keybordWillHide() {
        UIView.animate(withDuration: 0.25) {
            self.scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }

    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            UIView.animate(withDuration: 0.25) {
                self.scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardHeight, right: 0)
            }
            print(keyboardHeight)
        }
//        notification.userInfo?[UIKeyboardFrameEndUserInfoKey]
//        print(notification.userInfo)
    }

    @IBAction func tapAction(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
        print(scrollView.contentSize,
              scrollView.contentOffset)
    }
}

