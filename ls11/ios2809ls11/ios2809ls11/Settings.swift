//
//  Settings.swift
//  ios2809ls11
//
//  Created by WA on 29.10.2020.
//

import Foundation

//class NewSettings: Settings {
//
//}

final class Settings {

    private init() { }
    private let defaults = UserDefaults.standard

    static var shared = Settings()

    var appStartCount: Int {
        get {
            return defaults.integer(forKey: "appStartCount")
        }
        set {
            defaults.setValue(newValue, forKey: "appStartCount")
        }
    }
}
