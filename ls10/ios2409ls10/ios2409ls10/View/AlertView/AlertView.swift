//
//  AlertView.swift
//  ios2409ls10
//
//  Created by WA on 27.10.2020.
//

import UIKit

enum AlertType {
    case positive, negative

    var imageName: String {
        switch self {
        case .positive: return "positiveImage"
        case .negative: return "negativeImage"
        }
    }

    var buttonTitle: String {
        switch self {
        case .positive: return "Accept"
        case .negative: return "Cancel"
        }
    }

    var color: UIColor {
        switch self {
        case .positive: return .green
        case .negative: return .red
        }
    }
}

class AlertView: UIView {

    @IBOutlet weak var topImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!

    private var buttonActionHandler: (() -> Void)?

    func setupUI(type: AlertType, title: String, subtitle: String, actionHandler: (() -> Void)?) {
        topImageView.image = UIImage(named: type.imageName)
        titleLabel.text = title
        subtitleLabel.text = subtitle
        actionButton.setTitle(type.buttonTitle, for: .normal)
        actionButton.backgroundColor = type.color
        actionButton.layer.cornerRadius = 12
        buttonActionHandler = actionHandler
    }

    @IBAction func buttonAction(_ sender: UIButton) {
        buttonActionHandler?()
    }
}

extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle(for: T.self).loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}
