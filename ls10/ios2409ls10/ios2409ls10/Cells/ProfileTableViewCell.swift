//
//  ProfileTableViewCell.swift
//  ios2409ls10
//
//  Created by WA on 27.10.2020.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileTitleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!

    func setup(with student: StudentProtocol) {
        profileImageView.image = UIImage(named: student.profileImageName)
        profileTitleLabel.text = student.nickname
        subtitleLabel.text = student.name
    }
}
