//
//  ViewController.swift
//  ios2409ls10
//
//  Created by WA on 27.10.2020.
//

import UIKit

protocol StudentProtocol {
    var name: String { get set }
    var nickname: String { get set }
    var profileImageName: String { get set }
}

//struct Student: StudentProtocol {
//    var name: String
//
//    var nickname: String
//
//    var profileImageName: String
//
////    let name: String
////    let nickmane: String
////    let profileImageName: String
//}

//class AndroidStudent: StudentProtocol {
//
//}

class iOSStudent: StudentProtocol, Codable {
    var nickname: String
    var profileImageName: String
    var name: String

    func makeiPhoneApp() {
        
    }
    init(name: String, nickname: String, profileImageName: String) {
        self.name = name
        self.nickname = nickname
        self.profileImageName = profileImageName
    }
}

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    private let saver = LocalSaver()
    private var students: [iOSStudent] = [
//        iOSStudent(name: "Ihor", nickname: "Ih0r", profileImageName: "img1"),
//        iOSStudent(name: "Alla", nickname: "allys1k", profileImageName: "img2"),
//        iOSStudent(name: "Inna", nickname: "innys1k", profileImageName: "img3"),
//        iOSStudent(name: "Anton", nickname: "tosha", profileImageName: "img4")
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        readStudents()
//        var stStudent = Student(name: "Artem", nickmane: "Bazinga")
//        stStudent.name = "Ihor"
//
//        let clStudent = iOSStudent(name: "Ivan", nickname: "Van4a")
//        let clStudentTwo = clStudent
//        clStudent.name = "Anton"
//        print(clStudent.name, clStudentTwo.name)
        tableView.dataSource = self
        let nib = UINib(nibName: "ProfileTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "ProfileTableViewCell")
//        saver.save(students: students)
    }

    @IBAction func showAlert(_ sender: UIBarButtonItem) {
//        let alertController = UIAlertController(title: "Error", message: "Try again", preferredStyle: .alert)
//        let okAction = UIAlertAction(title: "Ok", style: .cancel) { action in
//            print("OK pressed")
//        }
//        let deleteAction = UIAlertAction(title: "Remove", style: .destructive) { action in
//            print("Remove")
//        }
//        alertController.addAction(okAction)
//        alertController.addAction(deleteAction)
////        alertController.preferredAction = deleteAction
//        present(alertController, animated: true, completion: nil)
        let vc = UIViewController()
        let alertView: AlertView = .fromNib()
        alertView.setupUI(type: .positive, title: "Congratulations", subtitle: "You have passed all tests with excellent mark!\n You are awesome iOS developer!") {
            vc.dismiss(animated: true, completion: nil)
        }
        vc.view.addSubview(alertView)
        alertView.center = vc.view.center

        vc.modalPresentationStyle = .overFullScreen
        vc.view.backgroundColor = .clear
        vc.modalTransitionStyle = .crossDissolve

        present(vc, animated: true, completion: nil)
    }
    
    private func readStudents() {
        saver.readStudents { students in
            self.students = students
            self.tableView.reloadData()
        }
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return students.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell", for: indexPath) as? ProfileTableViewCell else { fatalError() }
        cell.setup(with: students[indexPath.row])
        return cell
    }
}
