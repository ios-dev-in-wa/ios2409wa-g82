//
//  LocalSaver.swift
//  ios2409ls10
//
//  Created by WA on 27.10.2020.
//

import Foundation

class LocalSaver {

    private let fileManager = FileManager.default

    private var documentsUrl: URL {
        fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
    }
//    private let studentsUrl = URL(fileURLWithPath: "/Users/wa/Desktop/sss.txt")
    private let studentsFile = "StudentsiOSGroup.txt"

    func save(students: [iOSStudent]) {
        do {
            let encodedData = try JSONEncoder().encode(students)
            try encodedData.write(to: documentsUrl.appendingPathComponent(studentsFile))
        } catch {
            print(error.localizedDescription)
        }
    }

    func readStudents(closure: (([iOSStudent]) -> Void)) {
        do {
            let encoderData = try Data(contentsOf: documentsUrl.appendingPathComponent(studentsFile))
            let students = try JSONDecoder().decode([iOSStudent].self, from: encoderData)
            closure(students)
        } catch {
            print(error.localizedDescription)
        }
    }
    
}
