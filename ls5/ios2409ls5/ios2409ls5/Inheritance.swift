//
//  Inheritance.swift
//  ios2409ls5
//
//  Created by WA on 08.10.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class Animal {
    let name: String
    private(set) var furColor: UIColor

    init(name: String, furColor: UIColor) {
        self.name = name
        self.furColor = furColor
    }

    func eat() {
        print("\(name) eat something")
    }

    func changeFurColor(color: UIColor) {
        if color != furColor {
            furColor = color
        } else {
            print("You try to change color to the same")
        }
    }
}

class Rabbit: Animal, Season {

    let cutieLevel: Int

    init(cutieLevel: Int, name: String, furColor: UIColor) {
        self.cutieLevel = cutieLevel
        super.init(name: name, furColor: furColor)
    }

    override func eat() {
        super.eat()
        print("Cute \(name) eat something")
    }

    func runFast() {
        print("Cute \(name) running as a light")
    }
    
    func winterIsComming() {
        print("Rabbit \(name) change his fur to white")
        changeFurColor(color: .white)
    }
    
    func springIsComming() {
        print("Rabbit \(name) change his fur to gray")
        changeFurColor(color: .white)
    }
    
    func summerIsComming() {
        
    }
    
    func autumnIsComming() {
        
    }
    
}

class Bear: Animal, Season {
    var fatAmout = 0.0
    func winterIsComming() {
        print("Bear \(name) suck lapa")
        fatAmout -= 1
    }
    
    func springIsComming() {
        print("Bear \(name) looking for food")
        fatAmout += 1
    }
    
    func summerIsComming() {
        
    }
    
    func autumnIsComming() {
        
    }
    
}

protocol Season {
    func winterIsComming()
    func springIsComming()
    func summerIsComming()
    func autumnIsComming()
}
