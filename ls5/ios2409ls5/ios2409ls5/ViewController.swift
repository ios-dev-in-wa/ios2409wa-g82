//
//  ViewController.swift
//  ios2409ls5
//
//  Created by WA on 08.10.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var centerView: UIView!
    @IBOutlet weak var mainButton: UIButton!

    let someAnimal = Animal(name: "Some animal", furColor: .black)
    let rabbit = Rabbit(cutieLevel: 10, name: "Rabbit", furColor: .black)
    let bear = Bear(name: "Bennie", furColor: .brown)
    var arrayOfAnimals: [Animal] = []
    let box = UIView()

    override func viewDidLoad() {
        super.viewDidLoad()
        arrayOfAnimals.append(someAnimal)
        arrayOfAnimals.append(rabbit)
        arrayOfAnimals.append(bear)

        arrayOfAnimals.forEach { //animal in
            print($0.name)
        }
        print("------")
        let seasonAnimals = arrayOfAnimals.compactMap { $0 as? Season }
        seasonAnimals.forEach { $0.winterIsComming() }
        print("------")
        // Do any additional setup after loading the view.
        someAnimal.eat()
//        someAnimal.runFast()
        
        rabbit.eat()
        rabbit.runFast()

        view.backgroundColor = rabbit.furColor
//        rabbit.furColor = .red
        
//        box.frame = CGRect(x: 20, y: 20, width: 100, height: 100)
//        box.backgroundColor = .red
//        view.addSubview(box)
        mainButton.setTitle("WOW", for: .highlighted)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        let half = box.frame.width / 2
//        let xPos = CGFloat.random(in: half...view.frame.maxX - half)
//        let yPos = CGFloat.random(in: half...view.frame.maxY - half)
//        box.center = CGPoint(x: xPos, y: yPos)
        centerView.backgroundColor = UIColor.random
    }

    @IBAction func mainButtonAction(_ sender: UIButton) {
        sender.backgroundColor = .red
        print("BUTTON PRESSED")
    }

    func runSeasons() {
        
    }
}

extension CGFloat {
    static var random: CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

extension UIColor {
    static var random: UIColor {
        return UIColor(red: .random, green: .random, blue: .random, alpha: 1.0)
    }
}
