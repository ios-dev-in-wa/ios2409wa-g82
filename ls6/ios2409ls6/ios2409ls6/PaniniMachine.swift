//
//  PaniniMachine.swift
//  ios2409ls6
//
//  Created by WA on 13.10.2020.
//

import Foundation

class PaniniMachine {
    
    enum PaniniType: Int {
        case withMeat, blueCheese, vegan

        var name: String {
            switch self {
            case .blueCheese: return "King size panini with blue cheese"
            case .withMeat: return "Middle size panini with sausage"
            case .vegan: return "Little size panini with vegetables"
            }
        }

        var beansCount: Double {
            return 100
        }
    }

    private var purchaseCount = 0
    var isGrillOn = true
    var stock: [PaniniType: Int] = [:]

    func prepare(panini: PaniniType, count: Int) -> String {
        guard count > 0 else {
            return "Please choose quantity!"
        }
        if let paniniCount = stock[panini], paniniCount >= count {
            stock[panini] = paniniCount - count
            purchaseCount += 1
            return "Wow! Your \(count) \(panini.name) is ready.\nCarefull it's hot!"
        } else {
            return "Sorry we don't have\n\(count) \(panini.name)"
        }
    }

    func fillStock() -> String {
        stock[.blueCheese] = 10
        stock[.vegan] = 10
        stock[.withMeat] = 10
        return "Stock is filled!"
    }

    func sendComplain(value: String) -> String {
        guard purchaseCount > 0 else {
            return "Make a purchase first!"
        }
        print(value)
        return "Complain sent!"
    }
    
}
