//
//  ViewController.swift
//  ios2409ls6
//
//  Created by WA on 13.10.2020.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var veganButton: UIButton!
    @IBOutlet weak var meatButton: UIButton!
    @IBOutlet weak var blueCheeseButton: UIButton!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var complainTextField: UITextField!
    @IBOutlet weak var stepper: UIStepper!
    @IBOutlet weak var grillSwitch: UISwitch!
    @IBOutlet weak var grillView: UIView!
    
    private let paniniMachine = PaniniMachine()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        countLabel.layer.borderWidth = 1
        countLabel.layer.cornerRadius = 10
        grillView.layer.cornerRadius = 14
//        print(paniniMachine.prepare(panini: .blueCheese))
//        paniniMachine.fillStock()
//        print(paniniMachine.prepare(panini: .vegan))
            // Make keyboard visible and attached to textfield
//        complainTextField.becomeFirstResponder()
    }

    @IBAction func grillSwitchAction(_ sender: UISwitch) {
        paniniMachine.isGrillOn = sender.isOn
        grillView.backgroundColor = sender.isOn ? .red : .green
    }

    @IBAction func stepperValueChanged(_ sender: UIStepper) {
        countLabel.text = "\(Int(sender.value))"
    }

    @IBAction func preparePaniniAction(_ sender: UIButton) {
//        var statusText = ""
        // First way
//        switch sender {
//        case veganButton: statusText = paniniMachine.prepare(panini: .vegan)
//        case meatButton: statusText = paniniMachine.prepare(panini: .withMeat)
//        case blueCheeseButton: statusText = paniniMachine.prepare(panini: .blueCheese)
//        default: print("UNEXPECTED BUTTON")
//        }
        
        // Second way
        let tag = sender.tag
        if let panini = PaniniMachine.PaniniType(rawValue: tag) {
            statusLabel.text = paniniMachine.prepare(panini: panini, count: Int(stepper.value))
        } else {
            print("ERROR UNEXPECTED PANINI TYPE")
        }
//        paniniMachine.prepare(panini: .vegan)
    }

    @IBAction func fillStockAction(_ sender: UIButton) {
        statusLabel.text = paniniMachine.fillStock()
    }

    @IBAction func sendComplainAction(_ sender: UIButton) {
        guard let text = complainTextField.text, !text.isEmpty else { return }
        statusLabel.text = paniniMachine.sendComplain(value: text)
        complainTextField.text = ""
        // Make keyboard hidden and de-attached from textfield
        complainTextField.resignFirstResponder()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}

