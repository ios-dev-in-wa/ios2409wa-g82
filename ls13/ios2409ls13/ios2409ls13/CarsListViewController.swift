//
//  CarsListViewController.swift
//  ios2409ls13
//
//  Created by WA on 05.11.2020.
//

import UIKit

struct Car: Codable {
    let objectId: String
    let name: String
    let productionYear: Int
    let wasInAccident: Bool
}

class CarsListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    private var cars: [Car] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        fetchCars()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == "showAddNewCar", let vc = segue.destination as? AddNewCarViewController {
            vc.delegate = self
            if let indexPath = tableView.indexPathForSelectedRow {
                vc.isInEdit = true
                vc.car = cars[indexPath.row]
            }
        }
    }

    private func fetchCars() {
        CarService.shared.getCars { [weak self] cars in
            DispatchQueue.main.async {
                self?.cars = cars
                self?.tableView.reloadData()
            }
        }
    }

    private func removeCar(at indexPath: IndexPath) {
        CarService.shared.removeCar(cars[indexPath.row].objectId) { [weak self] in
            DispatchQueue.main.async {
                self?.showDeletionAlert {
                    self?.cars.remove(at: indexPath.row)
                    self?.tableView.deleteRows(at: [indexPath], with: .automatic)
                }
            }
        }
    }

    private func showDeletionAlert(completion: @escaping (() -> Void)) {
        let alert = UIAlertController(title: "Are you sure?", message: "Do you want to delete this item?", preferredStyle: .alert)
        let action = UIAlertAction(title: "Delete", style: .destructive, handler: { _ in
            completion()
        })
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(action)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
}

extension CarsListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cars.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CarCell", for: indexPath)
        let car = cars[indexPath.row]
        cell.textLabel?.text = car.name
        cell.detailTextLabel?.text = "\(car.productionYear) \(car.wasInAccident ? "Was broken" : "Like new")"
        return cell
    }
}

extension CarsListViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showAddNewCar", sender: nil)
        tableView.deselectRow(at: indexPath, animated: true)
    }

    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }

    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let action = UIContextualAction(style: .destructive, title: "Delete") { [weak self] (_, _, _) in
            self?.removeCar(at: indexPath)
        }
        return UISwipeActionsConfiguration(actions: [action])
    }

}

extension CarsListViewController: AddNewCarViewControllerDelegate {
    func newCarAdded(car: Car) {
        if cars.first(where: { $0.objectId == car.objectId}) == nil {
            cars.append(car)
            tableView.insertRows(at: [IndexPath(row: cars.count - 1, section: 0)], with: .automatic)
        } else {
            guard let index = cars.firstIndex(where: { $0.objectId == car.objectId }) else { return }
            cars.remove(at: index)
            cars.insert(car, at: index)
            tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
        }
    }
}
