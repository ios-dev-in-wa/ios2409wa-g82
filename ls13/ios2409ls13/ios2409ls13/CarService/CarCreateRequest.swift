//
//  CarCreateRequest.swift
//  ios2409ls13
//
//  Created by WA on 05.11.2020.
//

import Foundation

struct CarCreateRequest: Encodable {
    let name: String
    let productionYear: Int
    let wasInAccident: Bool
}
