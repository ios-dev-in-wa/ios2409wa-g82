//
//  CarService.swift
//  ios2409ls13
//
//  Created by WA on 05.11.2020.
//

import Foundation

final class CarService {

    private init() {}

    static let shared = CarService()

    private let basicUrl = URL(string: "https://parseapi.back4app.com/classes/Car")!

    private let session = URLSession.shared

    func getCars(completion: @escaping (([Car]) -> Void)) {

        var request = prepareRequest(url: basicUrl)
        request.httpMethod = "GET"

        session.dataTask(with: request) { data, response, error in
            if let error = error {
                print(error.localizedDescription)
            }
            guard let response = response as? HTTPURLResponse else { return }
            if response.statusCode == 200 {
                guard let data = data else { return }
                guard let responseData = try? JSONDecoder().decode(CarResponse.self, from: data) else {
                    print("Can't decode \(data)")
                    return
                }
                completion(responseData.results)
            }
        }.resume()
        
    }

    func postCar(_ carTitle: String, productionYear: Int, wasInAccident: Bool, completion: @escaping ((Car) -> Void)) {
        var request = prepareRequest(url: basicUrl)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"

        request.httpBody = try? JSONEncoder().encode(CarCreateRequest(name: carTitle, productionYear: productionYear, wasInAccident: wasInAccident))

        session.dataTask(with: request) { (data, response, error) in
            if let error = error {
                print(error.localizedDescription)
            }
            guard let response = response as? HTTPURLResponse else { return }
            if response.statusCode == 201 {
                guard let data = data else { return }
                guard let responseData = try? JSONDecoder().decode(CarCreateResponse.self, from: data) else {
                    print("Can't decode \(data)")
                    return
                }
                completion(Car(objectId: responseData.objectId, name: carTitle, productionYear: productionYear, wasInAccident: wasInAccident))
            }
        }.resume()
    }

    func updateCar(carId: String, carTitle: String, productionYear: Int, wasInAccident: Bool, completion: @escaping ((Car) -> Void)) {
        var request = prepareRequest(url: basicUrl.appendingPathComponent(carId))
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "PUT"

        session.dataTask(with: request) { data, response, error in
            if let error = error {
                print(error.localizedDescription)
            }
            guard let response = response as? HTTPURLResponse else { return }
            if response.statusCode == 200 {
                print(String(data: data!, encoding: .utf8))
                completion(Car(objectId: carId, name: carTitle, productionYear: productionYear, wasInAccident: wasInAccident))
            }
        }.resume()
    }

    func removeCar(_ carId: String, completion: @escaping (() -> Void)) {
        var request = prepareRequest(url: basicUrl.appendingPathComponent(carId))
        print(request.url)
        request.httpMethod = "DELETE"

        session.dataTask(with: request) { data, response, error in
            if let error = error {
                print(error.localizedDescription)
            }
            guard let response = response as? HTTPURLResponse else { return }
            if response.statusCode == 200 {
                completion()
            }
        }.resume()
    }

    private func prepareRequest(url: URL) -> URLRequest {
        var request = URLRequest(url: url)
        request.addValue("ZSj9FVTrKT3Q5wV01I2UUO8o3ezwcXK3r7aX6pay", forHTTPHeaderField: "X-Parse-Application-Id")
        request.addValue("mdJvnAk1zKkTTTgS80marWzK6oeOQQuUH0PXFesQ", forHTTPHeaderField: "X-Parse-REST-API-Key")

        return request
    }
}

