//
//  CarCreateResponse.swift
//  ios2409ls13
//
//  Created by WA on 05.11.2020.
//

import Foundation

struct CarCreateResponse: Codable {
    let objectId: String
    let createdAt: String
}
