//
//  CarResponse.swift
//  ios2409ls13
//
//  Created by WA on 05.11.2020.
//

import Foundation

struct CarResponse: Decodable {
    let results: [Car]
}
