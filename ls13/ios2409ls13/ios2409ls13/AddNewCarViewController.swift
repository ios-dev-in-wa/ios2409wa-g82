//
//  AddNewCarViewController.swift
//  ios2409ls13
//
//  Created by WA on 05.11.2020.
//

import UIKit

protocol AddNewCarViewControllerDelegate: class {
    func newCarAdded(car: Car)
}

class AddNewCarViewController: UIViewController {

    @IBOutlet weak var carTitleTextField: UITextField!
    @IBOutlet weak var productionYearTextField: UITextField!
    @IBOutlet weak var wasInAccidentSwitch: UISwitch!

    var delegate: AddNewCarViewControllerDelegate?
    var car: Car?
    var isInEdit = false

    override func viewDidLoad() {
        super.viewDidLoad()
        if let car = car {
            carTitleTextField.text = car.name
            productionYearTextField.text = "\(car.productionYear)"
            wasInAccidentSwitch.isOn = car.wasInAccident
            toggleEdit(isOn: false)
        }
    }

    private func toggleEdit(isOn: Bool) {
        isInEdit = isOn
        carTitleTextField.isEnabled = isOn
        productionYearTextField.isEnabled = isOn
        wasInAccidentSwitch.isEnabled = isOn
        navigationItem.rightBarButtonItem?.title = isOn ? "Save" : "Edit"
    }

    @IBAction func saveAction(_ sender: UIBarButtonItem) {
        if !isInEdit, car != nil {
            toggleEdit(isOn: true)
        } else {
            guard let title = carTitleTextField.text, !title.isEmpty else {
                showAlert(text: "Cat title should be more than 1 symbol")
                return
            }
            guard let year = productionYearTextField.text, let yearInt = Int(year) else {
                showAlert(text: "Production year must be Int")
                return
            }
            if let car = car {
                CarService.shared.updateCar(carId: car.objectId, carTitle: title, productionYear: yearInt, wasInAccident: wasInAccidentSwitch.isOn) { [weak self] car in
                    DispatchQueue.main.async {
                        self?.saveAndDismiss(car: car)
                    }
                }
            } else {
                CarService.shared.postCar(title, productionYear: yearInt, wasInAccident: wasInAccidentSwitch.isOn) { [weak self] car in
                    DispatchQueue.main.async {
                        self?.saveAndDismiss(car: car)
                    }
                }
            }
        }
    }

    private func saveAndDismiss(car: Car) {
        delegate?.newCarAdded(car: car)
        navigationController?.popViewController(animated: true)
    }

    private func showAlert(text: String) {
        let alert = UIAlertController(title: "Error", message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
