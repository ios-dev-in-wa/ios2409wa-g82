//
// ViewController.swift
// Lesson03_start
//
// Created by 3droot on 01.10.2020.
//
import UIKit

class ViewController2: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //    compare(number1: 2, number2: 2)
        //    compare(number1: 1, number2: 7)
        //    compare(number1: 5, number2: 3)
        //
        //    square(number: 4)
        //
        //   findDividers(number: 8)
        //  intDouble()
        //  stringExample()
        //   arrayExample()
        //   print( valueFor(index: 2))
        //   print( valueFor(index: 10))
        //  arrayExample()
        //  dictionaryExample()
        foo(money: 200, age: 17)
    }
    func compare(number1: Double, number2: Double ){
        if number1>number2 {
            print("number1 > number2")
        } else if number1 < number2 {
            print("number1 < number2")
        } else {
            print("Numbers are equal")
        }
    }
    func square(number: Int){
        let square = number * number
        print(square)
    }
    func findDividers(number: Int){
        var counter = 0
        //   var value = 1
        for i in 1 ... number {
            if number % i == 0 {
                counter += 1
                print ("NUmber \(i)")
            }
        }
        print("\(number) has \(counter) dividers")
    }
    func intDouble(){
        let value: Double = 0
        if value.isFinite{
            print("Finite")
        }
        let randomValue = Int.random(in: 0...10)
        print(randomValue)
        let zero = Int.zero
        print(zero)
        print(Double.pi)
        print(Int.max, Int.min)
    }
    func stringExample(){
        var helloWorld = "Hello world"
        helloWorld.append("C")
        print (helloWorld, helloWorld.count)
        print(helloWorld.hasPrefix("H"))
        print(helloWorld.contains(" "))
        //  print(helloWorld.replacingOccurrences(of: "o", with: "OoOOooO"))
    }
    func arrayExample(){
        //    let intArray = [1,4,5]
        var intArray: [Int] = [1,4,6]
        intArray.append(5)
        print(intArray)
        intArray.insert(100, at: 2)
        print(intArray)
        print(intArray[0])
        intArray.remove(at: 0)
        if intArray.contains(100) {
            print("100 is in array")
        }
        for value in intArray {
            print(value)
        }
        intArray.forEach { value in
            print(value)
        }
        let stringArray = intArray.map { value -> String in
            return "\(value)"
        }
        print(stringArray)
        let newArray = intArray.map { value -> String? in
            if value == 100 {
                return "\(value * 2)"
            } else {
                return nil
            }
        }
        print(newArray)
        let filtered = intArray.filter({ value -> Bool in
            return value % 2 == 0
        })
        print(filtered)
        print(intArray.sorted(by: <))
    }
    func valueFor(index: Int)-> String? {
        let stringArray = ["Artem", "Dima", "Oleg"]
        if index < stringArray.count {
            return stringArray[index]
        } else {
            return nil
        }
    }
    func dictionaryExample(){
        var contactBook = [
        "911": "emergency",
        "2020": "Pizza"
        ]
        contactBook["333"] = "Bank"
        print(contactBook["911"])
        print(contactBook)
        print(contactBook.keys)
        let someBook = [
            1: ["One", "Uno"],
            2: ["two", "Duo"]
        ]
        print(someBook)
        print(someBook[1]?.first?.hasPrefix("22"))
        print(someBook.values)
        someBook.forEach { key, value in
            print (key, value)
        }
    }
    func foo(money: Int, age: Int){
        if money > 100, age < 18 {
            print("good")
        } else {
            print ("bad")
        }
        print (money>100, age < 18 ? "good" : "bad")
    }
}
