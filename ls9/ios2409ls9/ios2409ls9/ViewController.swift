//
//  ViewController.swift
//  ios2409ls9
//
//  Created by WA on 22.10.2020.
//

import UIKit

struct Student {
    let name: String
    let nickname: String
}

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var students = [Student(name: "Artem", nickname: "@bazinga")]

    var arrayOfNames = ["Artem", "Ilya"]

    override func viewDidLoad() {
        super.viewDidLoad()
//        let tableView = UITableView()
        // Do any additional setup after loading the view.
//        tableView.delegate = self
        tableView.dataSource = self

        let nib = UINib(nibName: "UserTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "UserTableViewCell")
    }

    @IBAction func addAction(_ sender: UIBarButtonItem) {
        arrayOfNames.append("Ihor")
        // TO RELOAD WHOLE TABLE VIEW
        tableView.reloadData()
        // TO ADD NEW ELEMENT
//        tableView.insertRows(at: [IndexPath(row: arrayOfNames.count - 1, section: 0)], with: .automatic)
    }

    @IBAction func editAction(_ sender: UIBarButtonItem) {
        tableView.setEditing(!tableView.isEditing, animated: true)
    }
}

extension ViewController: UITableViewDelegate {
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return (indexPath.row % 2) == 0 ? 60 : 160
//    }
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        print("DISCLOSURE TAPPED")
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("User choose \(arrayOfNames[indexPath.row])")
    }

    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }

    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "To trash"
    }

    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let action = UIContextualAction(style: .destructive, title: "Deletion") { action, view, _ in
            print("REMOVE")
        }
        action.image = UIImage(systemName: "trash.fill")
        let actionTwo = UIContextualAction(style: .normal, title: "Mark as Favourite") { action, view, _ in
            print("Favourite")
        }
        actionTwo.backgroundColor = .yellow
        let configuration = UISwipeActionsConfiguration(actions: [action, actionTwo])
        return configuration
    }
}

extension ViewController: UITableViewDataSource {
    
    // REQUIRED
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfNames.count
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Section \(section)"
    }

    // OPTINAL
    func numberOfSections(in tableView: UITableView) -> Int { 1 }

    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return "Footer \(section)"
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print(indexPath.section, indexPath.row)
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "UserTableViewCell", for: indexPath) as? UserTableViewCell else {
            fatalError()
        }
        let student = students[0]
        print(student.name, student.nickname)
//        let intValue = Int.random(in: 0...100)
//        cell.textLabel?.text = String(intValue)
//        cell.detailTextLabel?.text = "\(100 - intValue)"
        let item = arrayOfNames[indexPath.row]
        cell.setupWith(name: "\(item) \(indexPath.row)", surname: "SURNAME")
        return cell
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        switch editingStyle {
        case .delete:
            arrayOfNames.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
            print("Delete")
        case .insert:
            print("Insert")
        case .none:
            print("None")
        }
    }

    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let value = arrayOfNames[sourceIndexPath.row]
        arrayOfNames.remove(at: sourceIndexPath.row)
        arrayOfNames.insert(value, at: destinationIndexPath.row)
        print(arrayOfNames)
    }
}
