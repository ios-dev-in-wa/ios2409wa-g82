//
//  UserTableViewCell.swift
//  ios2409ls9
//
//  Created by WA on 22.10.2020.
//

import UIKit

class UserTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var surnameLabel: UILabel!

    func setupWith(name: String, surname: String) {
        nameLabel.text = name
        surnameLabel.text = surname
    }
}
