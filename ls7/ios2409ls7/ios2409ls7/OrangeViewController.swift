//
//  OrangeViewController.swift
//  ios2409ls7
//
//  Created by WA on 15.10.2020.
//

import UIKit

enum TextSenderType {
    case orange, blue
}

protocol TextFieldDataDelegate: class {
    func textWasEdited(text: String)
//    var some: Bool { get set }
//    func textWasEdited(senderType: TextSenderType, text: String)
}

class OrangeViewController: UIViewController {

    var text: String?

    weak var delegate: TextFieldDataDelegate?
    @IBOutlet weak var textField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        view.backgroundColor = .brown
        print("TEXT: ",text)
    }

    @IBAction func mainButtonAction(_ sender: UIButton) {
        let blackVC = BlackViewController()
        navigationController?.pushViewController(blackVC, animated: true)
//        let identifier = Int.random(in: 0...1) == 0 ? "showGreenVC" : "showBlueVC"
//        performSegue(withIdentifier: identifier, sender: nil)
    }

    @IBAction func notMainButtonAction(_ sender: Any) {
        let viewController = UIViewController()
        viewController.view.backgroundColor = .red

        navigationController?.pushViewController(viewController, animated: true)
    }

    @IBAction func sendButtonAction(_ sender: Any) {
        guard let text = textField.text else { return }
        delegate?.textWasEdited(text: text)
    }
}
