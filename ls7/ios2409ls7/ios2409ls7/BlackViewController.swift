//
//  BlackViewController.swift
//  ios2409ls7
//
//  Created by WA on 15.10.2020.
//

import UIKit

class BlackViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        drawCircle()
    }
    
    func drawCircle() {
        let circleView = UIView()
        circleView.backgroundColor = .red
        circleView.frame = CGRect(x: 0, y: 0, width: 100, height: 100)

        circleView.layer.cornerRadius = 50

        view.addSubview(circleView)

        circleView.center = view.center
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        navigationController?.popViewController(animated: true)
    }
}
