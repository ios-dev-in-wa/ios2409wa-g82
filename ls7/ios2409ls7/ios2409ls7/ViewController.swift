//
//  ViewController.swift
//  ios2409ls7
//
//  Created by WA on 15.10.2020.
//

import UIKit

class ViewController: UIViewController {

    override func loadView() {
        super.loadView()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "123" {
            return false
        }
        return true
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        print(segue.identifier)
        switch segue.identifier {
        case "showOrangeVC":
            guard let destinationVC = segue.destination as? OrangeViewController else { return }
            destinationVC.delegate = self
            destinationVC.text = "HELLO ORANGE"
        case "showBlueVC":
            guard let navC = segue.destination as? UINavigationController,
                  let destinationVC = navC.viewControllers.first as? BlueViewController else { return }
//            _ = destinationVC.view // force view load
            destinationVC.addPlaceholder(text: "Write in me")
            destinationVC.didChangeText = { value in
                print("TEXT IS: --", value)
            }
//            destinationVC.delegate = self
        default: break
        }
    }

    @IBAction func pressMeAction(_ sender: Any) {
        print("I AM PRESSED")
    }
    deinit {
        
    }

}

extension ViewController: TextFieldDataDelegate {
    func textWasEdited(text: String) {
        print("This is a text:", text)
    }
}
