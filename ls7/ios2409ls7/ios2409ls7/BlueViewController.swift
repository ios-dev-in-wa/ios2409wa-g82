//
//  BlueViewController.swift
//  ios2409ls7
//
//  Created by WA on 15.10.2020.
//

import UIKit

//typealias MyCoolInt = Int

class BlueViewController: UIViewController {

    @IBOutlet weak var textField: UITextField!

    var didChangeText: ((String) -> Void)?

    var placeHolder = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        print(navigationController == nil ? "NO NAVC" : "HAVE NAVC")
        textField.placeholder = placeHolder
    }

    func addPlaceholder(text: String) {
        placeHolder = text
//        textField.placeholder = text
    }
    

    @IBAction func blueButtonAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    

    @IBAction func sendButtonAction(_ sender: Any) {
        guard let text = textField.text else { return }
//        delegate?.textWasEdited(text: text)
        didChangeText?(text)
    }
}
