class SwiftRobotControlCenter: RobotControlCenter {
    //  Level name setup
    override func viewDidLoad() {
        levelName = "L4H" //  Level name
//        levelName = "L3C"
        super.viewDidLoad()
    }

    override func run() {
        while rightIsClear {
            oneStepDown()
        }
        put()
        turn180()
        goToBlock()
        turn180()
        while leftIsClear {
            oneStepUp()
        }
        put()
    }

    func oneStepDown (){
        put()
        move()
        turnRight()
        move()
        turnLeft()
    }
    func turnLeft(){
        for _ in 0..<3 {
            turnRight()
        }
    }
    func goToBlock(){
        while frontIsClear {
            move()
        }
    }
    func turn180(){
        turnRight()
        turnRight()
    }
    func oneStepUp(){
        put()
        move()
        turnLeft()
        move()
        turnRight()
    }
}











