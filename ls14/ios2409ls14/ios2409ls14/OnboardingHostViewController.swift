//
//  OnboardingHostViewController.swift
//  ios2409ls14
//
//  Created by WA on 10.11.2020.
//

import UIKit

protocol OnboardingHostViewControllerDelegate: class {
    func onboardingIsSkipped()
}

class OnboardingHostViewController: UIViewController {

    @IBOutlet weak var pageControll: UIPageControl!
    weak var delegate: OnboardingHostViewControllerDelegate?
    private var pageVC: UIPageViewController?
    private let onboardingModels = [
        OnboardingModel(imageName: "image1", title: "Test one", subtitle: "Hello my dear friend"),
        OnboardingModel(imageName: "image2", title: "Test two", subtitle: "Hello my dear friend"),
        OnboardingModel(imageName: "image3", title: "Test three", subtitle: "You should like this app!"),
    ]

    private lazy var viewControllers: [UIViewController] = {
        return onboardingModels.map { model -> UIViewController in
            createVC(model: model)
        }
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        pageControll.numberOfPages = viewControllers.count
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == "showPageController", let pageVC = segue.destination as? UIPageViewController {
            self.pageVC = pageVC
            setupPageVC()
        }
    }

    private func setupPageVC() {
        guard let pageVC = pageVC else { return }
        pageVC.dataSource = self
        pageVC.delegate = self

        pageVC.setViewControllers([viewControllers.first!], direction: .forward, animated: true, completion: nil)
    }

    func createVC(model: OnboardingModel) -> OnboardingViewController {
        let vc = storyboard!.instantiateViewController(withIdentifier: "OnboardingViewController") as! OnboardingViewController
        _ = vc.view
        vc.setupWith(model: model)
        return vc
    }

    @IBAction func skipAction(_ sender: Any) {
        UserDefaults.standard.setValue(1, forKey: "isFirstLaunch")
        delegate?.onboardingIsSkipped()
    }
}

extension OnboardingHostViewController: UIPageViewControllerDelegate {

    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard let controller = pageViewController.viewControllers?.first else { return }
        pageControll.currentPage = viewControllers.firstIndex(of: controller) ?? 0
    }
}

extension OnboardingHostViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if let index = viewControllers.firstIndex(of: viewController) {
            if index == 0 {
                return nil
            } else {
                return viewControllers[index - 1]
            }
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if let index = viewControllers.firstIndex(of: viewController) {
            if index == viewControllers.count - 1 {
                return nil
            } else {
                return viewControllers[index + 1]
            }
        }
        return nil
    }
}
