//
//  OnboardingViewController.swift
//  ios2409ls14
//
//  Created by WA on 10.11.2020.
//

import UIKit

struct OnboardingModel {
    let imageName: String
    let title: String
    let subtitle: String
}

class OnboardingViewController: UIViewController {

    @IBOutlet weak var onboardingImageView: UIImageView!
    @IBOutlet weak var onboardingTitleLabel: UILabel!
    @IBOutlet weak var onboardingSubtitleLabel: UILabel!

    func setupWith(model: OnboardingModel) {
        onboardingImageView.image = UIImage(named: model.imageName)
        onboardingTitleLabel.text = model.title
        onboardingSubtitleLabel.text = model.subtitle
    }
}
