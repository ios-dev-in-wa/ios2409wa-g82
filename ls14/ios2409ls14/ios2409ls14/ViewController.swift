//
//  ViewController.swift
//  ios2409ls14
//
//  Created by WA on 10.11.2020.
//

import UIKit
//
//class LabelViewModel: ViewModel {
//    let textValue: String = ""
//}

//class ViewModel {
//    let color: UIColor = .red
//    let frame: CGRect = .zero
//}

class ViewController: UIViewController {

    @IBOutlet weak var profileImageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
//        let documentVC = UIDocumentPickerViewController()

        // Do any additional setup after loading the view.
    }

    @IBAction func imageTapAction(_ sender: UITapGestureRecognizer) {
        let alertController = UIAlertController(title: "Image source", message: nil, preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { [weak self] _ in
            self?.showImagePicker(sourceType: .camera)
        }
        alertController.addAction(cameraAction)

        let libraryAction = UIAlertAction(title: "Library", style: .default) { [weak self] _ in
            self?.showImagePicker(sourceType: .camera)
        }
        alertController.addAction(libraryAction)

        present(alertController, animated: true, completion: nil)
        
    }

    private func showImagePicker(sourceType: UIImagePickerController.SourceType) {
        let imageController = UIImagePickerController()
        imageController.delegate = self
        imageController.allowsEditing = true
        imageController.sourceType = sourceType
        present(imageController, animated: true, completion: nil)
    }
}

extension ViewController: UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            profileImageView.image = image
        } else if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            profileImageView.image = image
        }
        print(info)
        picker.dismiss(animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        print("cancel pressed")
    }
}
