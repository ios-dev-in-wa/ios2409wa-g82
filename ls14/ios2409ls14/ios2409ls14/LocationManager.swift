//
//  LocationManager.swift
//  ios2409ls14
//
//  Created by WA on 10.11.2020.
//

import Foundation
import CoreLocation

final class LocationManager: NSObject {

    static var shared = LocationManager()

    private override init() {
        super.init()
        manager.delegate = self
    }

    let manager = CLLocationManager()

    func requestAccess() {
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
    }
}

extension LocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print(locations)
    }
}
