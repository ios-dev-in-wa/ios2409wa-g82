//
//  ViewController.swift
//  ios2409ls2
//
//  Created by WA on 29.09.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        letVarTest()
//        castingTest()
//        print(getSquareOf(number: 10))
//        print(getSquareOf(number: 4))
//        logicTest()
//        counterExample()
//        greetingsFor(name: "Artem", count: 3)
//        print(isVegetable(vegetable: "meal meat"))
//        print(isVegetable(vegetable: "onion"))
//        print(isVegetable(vegetable: "onions"))
//        view.backgroundColor = UIColor.red
//        var counter: CGFloat = 0
//        for _ in 0..<4 {
//            counter += 1
//            drawBox(x: 50 * counter, y: 500, width: 2, height: 100)
//        }
        let blackBox = drawBox(x: 10, y: 10, width: 100, height: 100)
        
        let blueBox = drawBox(x: 80, y: 80, width: 50, height: 50)
        blueBox.backgroundColor = UIColor.blue
    }

    func letVarTest() {
        var ageContainer: Double = 10
        let constantAge = 20
        ageContainer = 120
        print(ageContainer, constantAge)

        let helloWorldString = "Hello world"
        print(helloWorldString)

        let boolValue: Bool = false // true
    }

    func castingTest() {
        let stringYear = "2020"
        let myAge = 40
        if let intYear = Int(stringYear) {
            print(intYear - myAge)
        }
        if let intYearTwo = stringYear as? Int {
            print(intYearTwo - myAge)
        }
    }

    func squareOfNumber() -> Int {
        let result = 10 * 10
        return result
    }

    func getSquareOf(number: Int) -> Int {
        let result = number * number
        return result
    }

    func logicTest() {
//        let result = 11111 + 2222 // * / - + %
        let resultTwo = 11 % 3
        print(resultTwo)

        let stringYear = "20"
        if let intYear = Int(stringYear) {
            let isEqual = 20 >= intYear // == > < <= >=
            if isEqual {
                print("YES")
            } else {
                print("NO")
            }
        }
    }

    func counterExample() {
        var counter = 0
        for _ in 0...10 {
            print(counter)
            counter += 1
//            counter = counter + 1
        }
        print("LAST")
        print(counter)
    }

    func greetingsFor(name: String, count: Int) {
        for _ in 0..<count {
            print("Hello, my dear: \(name)")
        }
    }

    func isVegetable(vegetable: String) -> Bool {
        switch vegetable {
        case "pickle": return true
        case "tomato": return true
        case "onion": return true
        default: return false
        }
    }
    
    func switchBoolExample(x: Int, y: Int) -> String {
        let result = x > y
        switch result {
        case true: return "X is bigger then Y"
        case false: return "Y is bigger then X"
        }
    }

    func switchExample(number: Int) -> String {
        switch number {
        case 0: return "ZERO"
        case 10: return "TEN"
        default: return "UNKNOWN NUMBER"
        }
    }

    func drawBox(x: CGFloat, y: CGFloat, width: CGFloat, height: CGFloat) -> UIView {
        let box = UIView()
        box.backgroundColor = UIColor.black
        box.frame = CGRect(x: x, y: y, width: width, height: height)
        box.layer.cornerRadius = 12
        view.addSubview(box)
        return box
    }
}

