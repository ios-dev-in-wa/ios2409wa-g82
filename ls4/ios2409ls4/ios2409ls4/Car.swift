//
//  Car.swift
//  ios2409ls4
//
//  Created by WA on 06.10.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

class Car {
    let bodyType: CarBodyType

    init(bodyType: CarBodyType) {
        self.bodyType = bodyType
    }
}

enum CarBodyType {
    case sedan, coope, hatchback
}
