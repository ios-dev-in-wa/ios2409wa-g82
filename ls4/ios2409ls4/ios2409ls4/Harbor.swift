//
//  Harbor.swift
//  ios2409ls4
//
//  Created by WA on 06.10.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

class Harbor {
    var ships: [Ship] = []
    var crewSize: Int = 10
    var pristanCount: Int = 1

    func printStatus() {
        print("Ships in harbor: \(ships.count), \(ships)")
    }

    func addShipToHarbor(ship: Ship) {
        ships.append(ship)
    }

    func shipIsGoingToLeave(ship: Ship) {
        ships.removeAll { value -> Bool in
            return value.name == ship.name
        }
    }

    func kickAll() {
        ships.removeAll()
        print("All ships kicked from Harbor")
    }
}
