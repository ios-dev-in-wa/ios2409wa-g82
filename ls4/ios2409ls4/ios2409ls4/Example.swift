//
//  Example.swift
//  ios2409ls4
//
//  Created by WA on 06.10.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class BattleShip: Ship {
    let cannonCount: Int

    init() {
        self.cannonCount = 10
        super.init(crewSize: 100)
    }
}

class Ship: NSObject {
    
    let name: String// = "Ship"
    let machtaCount: Int// = 2
    let crewSize: Int// = 10
    let bodyColor: UIColor
    var speed: Double = 0.0

    init(crewSize: Int) {
        self.name = "Ship"
        self.machtaCount = 1
        self.crewSize = crewSize
        self.bodyColor = .black
    }

    init(name: String, machtaCount: Int, crewSize: Int, bodyColor: UIColor) {
        self.name = name
        self.machtaCount = machtaCount
        self.crewSize = crewSize
        self.bodyColor = bodyColor
        print(self.name)
    }

    override var description: String {
        return "Ship with \(name) and crew count: \(crewSize)"
    }

    func makeAShot() {
        print("Making a shot")
    }

    func increaseSpeed() {
        speed += 1
    }

    func decreaseSpeed() {
        speed -= 1
    }
}
