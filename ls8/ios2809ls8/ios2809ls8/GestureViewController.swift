//
//  GestureViewController.swift
//  ios2809ls8
//
//  Created by WA on 20.10.2020.
//

import UIKit

class GestureViewController: UIViewController {

    @IBOutlet weak var panView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func didTap(_ sender: UITapGestureRecognizer) {
        print(sender.location(in: view))
        switch sender.state {
        case .began: print("BEGAN")
        case .ended: print("ENDED")
        default: print(sender.state.rawValue)
        }
        print(sender.state)
        view.backgroundColor = .random
    }

    @IBAction func panGestureAction(_ sender: UIPanGestureRecognizer) {
        panView.center = sender.location(in: view)
//        print("Translation", sender.translation(in: view))
//        print("Velocity", sender.velocity(in: view))
        switch sender.state {
        case .began: print("BEGAN")
        case .changed:
            print("Changed")
        case .ended: print("ENDED")
        default: print(sender.state.rawValue)
        }
        print("Location", sender.location(in: view))
    }

    
    @IBAction func pinchAction(_ sender: UIPinchGestureRecognizer) {
        print("Scale", sender.scale)
        print("Velocity", sender.velocity)
    }

    @IBAction func rotationAction(_ sender: UIRotationGestureRecognizer) {
        print("Rotation", sender.rotation)
    }

    @IBAction func swipeAction(_ sender: UISwipeGestureRecognizer) {
        print(sender.direction)
//        sender.state
    }

    @IBAction func edgeAction(_ sender: UIScreenEdgePanGestureRecognizer) {
        print(sender.edges)
    }

    @IBAction func longTapAction(_ sender: UILongPressGestureRecognizer) {
        sender.allowableMovement = 10
        switch sender.state {
        case .began: print("BEGAN")
        case .changed:
            print("Changed")
        case .ended: print("ENDED")
        default: print(sender.state.rawValue)
        }
    }
}

extension CGFloat {
    static var random: CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

extension UIColor {
    static var random: UIColor {
        return UIColor(red: .random, green: .random, blue: .random, alpha: 1.0)
    }
}
