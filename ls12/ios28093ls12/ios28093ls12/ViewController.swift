//
//  ViewController.swift
//  ios28093ls12
//
//  Created by WA on 03.11.2020.
//

import UIKit

struct Car: Codable {
    let name: String
    let productionYear: Int
    let wasInAccident: Bool
    let objectId: String
}

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    private let service = CarService()

    var cars: [Car] = [
//        Car(name: "Audi", productionYear: "1990", color: .gray),
//        Car(name: "BMW", productionYear: "1997", color: .red)
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        service.getCars { [weak self] cars in
            self?.cars = cars
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        tableView.dataSource = self
        tableView.delegate = self
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == "showCarsDetails", let navC = segue.destination as? UINavigationController,
           let vc = navC.viewControllers.first as? CarDetailsViewController,
           let selectedCar = sender as? Car {
//            print(tableView.indexPathForSelectedRow?.row)
            vc.selectedCar = selectedCar
        }
    }

//    override func performSegue(withIdentifier identifier: String, sender: Any?) {
//        super.performSegue(withIdentifier: identifier, sender: sender)
//        if identifier == "showDetails", let vc = segu
//    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showCarsDetails", sender: cars[indexPath.row])
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cars.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CarTableViewCell", for: indexPath)
        let car = cars[indexPath.row]
        cell.textLabel?.text = car.name
        cell.detailTextLabel?.text = "\(car.productionYear) " + "year"
        return cell
    }
}
