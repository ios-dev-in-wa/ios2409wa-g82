//
//  CarService.swift
//  ios28093ls12
//
//  Created by WA on 03.11.2020.
//

import Foundation

class CarService {

    private let session = URLSession.shared

    func getCars(completion: @escaping (([Car]) -> Void)) {
        guard let url = URL(string: "https://parseapi.back4app.com/classes/Car") else { return }
        var request = URLRequest(url: url)
        request.addValue("ZSj9FVTrKT3Q5wV01I2UUO8o3ezwcXK3r7aX6pay", forHTTPHeaderField: "X-Parse-Application-Id")
        request.addValue("mdJvnAk1zKkTTTgS80marWzK6oeOQQuUH0PXFesQ", forHTTPHeaderField: "X-Parse-REST-API-Key")
        request.httpMethod = "GET"

        let dataTask = session.dataTask(with: request) { (data, response, error) in
            if let error = error {
                print(error.localizedDescription)
            }

            guard let response = response as? HTTPURLResponse else { return }
            switch response.statusCode {
            case 200, 201:
//                print(String(data: data, encoding: .utf8))
                guard let data = data, let carResponse = try? JSONDecoder().decode(CarResposne.self, from: data) else { return }
                completion(carResponse.results)
            default: print("ERROR unexpected code \(response.statusCode)")
            }
        }
        dataTask.resume()
//        let query = PFQuery(className: "Car")
//        query.findObjectsInBackground { (objects, error) in
//            if let error = error {
//                print(error.localizedDescription)
//            }
//            print(objects?.first?["name"])
//        }
    }
}
