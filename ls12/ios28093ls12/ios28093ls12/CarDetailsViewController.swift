//
//  CarDetailsViewController.swift
//  ios28093ls12
//
//  Created by WA on 03.11.2020.
//

import UIKit

class CarDetailsViewController: UIViewController {

    var selectedCar: Car?
    @IBOutlet weak var productionYearLabel: UILabel!
    @IBOutlet weak var carNameLabel: UILabel!

    var bottomContraint: NSLayoutConstraint?

    override func viewDidLoad() {
        super.viewDidLoad()

        productionYearLabel.text = "\(selectedCar?.productionYear)"
        carNameLabel.text = selectedCar?.name
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        print(traitCollection.horizontalSizeClass.rawValue, traitCollection.verticalSizeClass.rawValue)
    }

    func updateConstraints(newTraits: UITraitCollection) {
        bottomContraint?.isActive = newTraits.verticalSizeClass == .compact
    }
}
