//
//  ViewController.swift
//  ios2409ls16
//
//  Created by WA on 17.11.2020.
//

import UIKit

//class UIElement {
//    enum UIElementType: String {
//        case label, imageView, view
//    }
//    let rect = CGRect.zero
//    let type: UIElementType
//}
//
//class UIImageElement: UIElement {
//    let image: UIImage?
//}
class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    private var persons = [Person]()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        DataBaseManager.shared.personManager.fetchPersons { [weak self] persons in
            self?.persons = persons
            self?.tableView.reloadData()
        }
        navigationItem.title = "titleLabelName".localized
//        view.superviews.forEach { sub in
//            var model: UIElement!
//            switch sub {
//            case is UILabel:
//                model = UIElement(type: .label)
//            case is UIView:
//                model = UIElement(type: .view)
//            case is UIImageView:
//                model = UIElement(type: .imageView)
//            default:
//            }
//
//        }
//        Locale.current
    }

    @IBAction func addNewPersonAction(_ sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: "Person name", message: "Please, enter person name", preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "Save", style: .default) { action in
            if let text = alertController.textFields?.first?.text {
                DataBaseManager.shared.personManager.createPerson(name: text, imageUrl: nil) { [weak self] person in
                    guard let self = self else { return }
                    self.persons.append(person)
                    self.tableView.insertRows(at: [IndexPath(row: self.persons.count - 1, section: 0)], with: .automatic)
                }
            }
        }
        alertController.addTextField()
        
        alertController.addAction(saveAction)
        present(alertController, animated: true, completion: nil)
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let action = UIContextualAction(style: .destructive, title: "Delete") { _, _, _ in
            DataBaseManager.shared.personManager.shared.delete(person: self.persons[indexPath.row], completion: {
                self.persons.remove(at: indexPath.row)
                self.tableView.reloadData()
            })
        }
        return UISwipeActionsConfiguration(actions: [action])
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return persons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PersonCell", for: indexPath)
        let person = persons[indexPath.row]
        cell.textLabel?.text = person.name?.localized
        cell.detailTextLabel?.text = person.imageUrl
        return cell
    }
}
