//
//  WebViewController.swift
//  ios2409ls16
//
//  Created by WA on 17.11.2020.
//

import UIKit
import WebKit
import SafariServices

class WebViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        

//        webView.evaluateJavaScript(<#T##javaScriptString: String##String#>, completionHandler: <#T##((Any?, Error?) -> Void)?##((Any?, Error?) -> Void)?##(Any?, Error?) -> Void#>)
        webView.load(URLRequest(url: URL(string: "https://www.google.com")!))
        let url = Bundle.main.url(forResource: "videoExample", withExtension: "mov")!
        webView.loadFileURL(url, allowingReadAccessTo: url)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        present(SFSafariViewController(url: URL(string: "https://www.google.com")!), animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
