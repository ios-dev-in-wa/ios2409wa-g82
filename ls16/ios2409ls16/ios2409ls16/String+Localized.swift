//
//  String+Localized.swift
//  ios2409ls16
//
//  Created by WA on 17.11.2020.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
