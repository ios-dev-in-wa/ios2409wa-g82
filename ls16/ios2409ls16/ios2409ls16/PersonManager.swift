//
//  PersonManager.swift
//  ios2409ls16
//
//  Created by WA on 17.11.2020.
//

import CoreData
import UIKit

final class DataBaseManager {
    private let context: NSManagedObjectContext

    let personManager: PersonManager
//    let petManager: PetManager

    private init() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        self.context = appDelegate.persistentContainer.viewContext
        self.personManager = PersonManager(context: context)
//        self.petManager = PetManager(context: context)
    }

    static let shared = DataBaseManager()

//    func save(elemets: [UIElement]) {
//        elemets.forEach { element in
////            element as? UIImageElement
////            imageManager.save(..)
//        }
//    }
}

class PersonManager {
    let context: NSManagedObjectContext

    init(context: NSManagedObjectContext) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        self.context = appDelegate.persistentContainer.viewContext
    }
//F0CF5B87-28D5-4B16-9861-3D616F9FAA05
    func createPerson(name: String, imageUrl: String?, completion: ((Person) -> Void)?) {
        let entity = NSEntityDescription.entity(forEntityName: "Person", in: context)!
        guard let object = NSManagedObject(entity: entity, insertInto: context) as? Person else { return }
        object.name = name
        object.imageUrl = imageUrl
        object.itemId = UUID().uuidString

        do {
            try context.save()
            completion?(object)
        } catch {
            print(error.localizedDescription)
        }
    }

    func fetchPersons(completion: (([Person]) -> Void)?) {
        let fetchRequest = NSFetchRequest<Person>(entityName: "Person")
//        fetchRequest.predicate = NSPredicate(format: "\(#keyPath(Person.itemId)) == %@", "F0CF5B87-28D5-4B16-9861-3D616F9FAA05")
        do {
            let persons = try context.fetch(fetchRequest)
            completion?(persons)
        } catch {
            print(error.localizedDescription)
        }
    }

    func delete(person: Person, completion: (() -> Void)?) {
        context.delete(person)
        completion?()
    }
}
